bits 32

global start        

extern exit, printf
import exit msvcrt.dll
import printf msvcrt.dll

; Enunt: Se da un numar natural negativ a (a: dword). 
;        Sa se afiseze valoarea lui in baza 10 si in baza 16, in urmatorul format: "a = <base_10> (baza 10), a = <base_16> (baza 16)"

segment data use32 class=data
    ; Variabile:

    a dd -10
    format db 'a = %d (baza10), a = %x (baza16)', 0
    
segment code use32 class=code
    start:
        ; Instructiuni:
        
        push dword [a]
        push dword [a]
        push dword format
        call [printf]
        add esp, 4*3
    
        push    dword 0
        call    [exit]