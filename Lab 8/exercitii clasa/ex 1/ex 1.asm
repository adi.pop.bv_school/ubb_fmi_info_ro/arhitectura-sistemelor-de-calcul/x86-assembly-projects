bits 32

global start        

extern exit
extern printf
import exit msvcrt.dll
import printf msvcrt.dll

; Enunt:

segment data use32 class=data
    ; Variabile:

    format db 'Ana are %d mere si %d pere', 0
    
segment code use32 class=code
    start:
        ; Instructiuni:
        
        push dword 7
        push dword 10
        push dword format
        call [printf]
        add esp, 4*3
    
        push    dword 0
        call    [exit]