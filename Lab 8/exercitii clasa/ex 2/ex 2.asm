bits 32

global start        

extern exit, scanf, printf
import exit msvcrt.dll
import scanf msvcrt.dll
import printf msvcrt.dll

; Enunt:

segment data use32 class=data
    ; Variabile:

    a dd 0
    b dd 0
    s dd 0
    format_out_a db 'Introduceti a: ', 0
    format_out_b db 'Introduceti b: ', 0
    format_out_sum db 'Suma dintre %d si %d este %d', 0
    format_in db '%d', 0
    
segment code use32 class=code
    start:
        ; Instructiuni:
        
        push dword format_out_a
        call [printf]
        add esp, 4*1
        push dword a
        push dword format_in
        call [scanf]
        add esp, 4*2
        
        push dword format_out_b
        call [printf]
        add esp, 4*1
        push dword b
        push dword format_in
        call [scanf]
        add esp, 4*2
        
        mov eax, [a]
        add eax, [b]
        mov [s], eax
        
        push dword [s]
        push dword [b]
        push dword [a]
        push dword format_out_sum
        call [printf]
        add esp, 4*3
    
        push    dword 0
        call    [exit]