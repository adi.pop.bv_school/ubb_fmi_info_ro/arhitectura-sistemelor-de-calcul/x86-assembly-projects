bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit               ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    ; ...
    
    a db 00111100b
    b dw 0000001111000000b
    c db 00000000b
    
; our code starts here
segment code use32 class=code
    start:
        ; ...
        ;Se dau un octet A si un cuvant B. Sa se obtina un octet C care are pe bitii 0-3 bitii 2-5 ai lui A, iar pe bitii 4-7 bitii 6-9 ai lui B.
        ; c = b9 b8 b7 b6 a5 a4 a3 a2
        
        mov al, [a] ; al = a7 a6 a5 a4 a3 a2 a1 a0
        mov cl, 2
        shr al, cl ; al = 0 0 a7 a6 a5 a4 a3 a2
        and al, 00001111b ; al = 0 0 0 0 a5 a4 a3 a2
        or byte[c], al ; c = 0 0 0 0 a5 a4 a3 a2
        
        mov ax, [b] ; ax = b15 b14 ... b0
        shr al, cl ; al = 0 0 b7 b6 b5 b4 b3 b2
        and al, 00110000b ; al = 0 0 b7 b6 0 0 0 0
        or byte[c], al ; c = 0 0 b7 b6 a5 a4 a3 a2
        
        mov cl, 6
        shl ah, cl ; ah = b9 b8 0 0 0 0 0 0
        or byte[c], ah ; c = b9 b8 b7 b6 a5 a4 a3 a2
        mov al, [c] ; rezultat pe al: 11111111b / FFh / 255
    
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
