bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit               ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    ; ...
    
    a db 2
    b db 3
    c db 4
    d dw 5
    
    two db 2

; our code starts here
segment code use32 class=code
    start:
        ; ...
        
        
        mov ah, 0
        mov al, [a]
        sub ax, [b]
        
        mul byte[two]
        
        mov dx, [d]
        sub dx, ax
        
        mov al, [b]
        mul byte[c]
        
        add ax, dx
        div byte[two]
        
        ;mov ax, [a]
        ;sub ax, [b]
        
        ;mov dl, 2
        ;mov dh, 0
        ;mul dl
        ;neg ax
        
        ;add ax, [d]
        ;mov dx, ax
        
        ;mov al, [b]
        ;mul byte[c]
        
        ;add ax, dx
        
        ;mov edx, 0
        ;mov dl, 2
        ;div dl 
        
        
    
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
