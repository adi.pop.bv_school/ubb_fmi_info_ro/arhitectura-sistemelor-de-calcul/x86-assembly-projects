bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit               ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    ; ...
    ; concatenarea a 2 siruri
    
    s db 1,2,3,4
    l_s equ $-s
    t db 6,7,8,9
    l_t equ $-tell
    d times  l_s+l_t db 0
    
; our code starts here
segment code use32 class=code
    start:
        ; ...
        
        mov esi, 0
        mov edi, 0
        mov ecx, l_s
        repeta:
            mov al, [s + esi]
            mov [d + edi], al
            inc edi
            mov al, [t + esi]
            mov [d + edi], al
            inc edi
            inc esi
        loop repeta
    
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
