bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit               ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    ; ...
    
    ; x - (a * b * 25 + c * 3) / (a + b) + e
    
    a db -2
    b db 3
    c db 4
    e dd 5
    x dq 6
    
    twentyfive db 25
    three db 3

; our code starts here
segment code use32 class=code
    start:
        ; ...
        
        mov al, [a] ; al = a
        imul byte[b] ; ax = (a*b)
        imul byte[twentyfive] ; ax = (a*b*25)
        
        mov dx, ax ; dx = (a*b*25)
        
        mov al, [c] ; al = c
        imul byte[three] ; ax = (c*3)
        
        add dx, ax ; dx = (a*b*25+c*3)
        
        mov al, [a] ; al = a
        add al, [b] ; al = (a+b)
        
        cbw ; ax = (a+b)
        mov cx, ax ; cx = (a+b)
        mov ax, dx ; ax = (a*b*25+c*3)
        cwd ; dx:ax = (a*b*25+c*3)
        idiv cx ; ax = (a*b*25+c*3)/(a+b)
        
        mov edx, [e] ; edx = e
        cwde ; eax = (a*b*25+c*3)/(a+b)
        neg eax ; eax = -(a*b*25+c*3)/(a+b)
        add eax, edx ; eax = -(a*b*25+c*3)/(a+b)+e
        
        cdq ; edx:eax = -(a*b*25+c*3)/(a+b)+e
        add eax, dword[x+0]
        adc edx, dword[x+4] ; rez => edx:eax = x-(a*b*25+c*3)/(a+b)+e
        
        
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
