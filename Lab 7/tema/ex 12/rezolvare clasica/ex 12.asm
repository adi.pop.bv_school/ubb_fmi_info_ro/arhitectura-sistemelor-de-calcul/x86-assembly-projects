bits 32

global start        

extern exit
import exit msvcrt.dll

; Enunt:Se da un sir A de dublucuvinte. Cunstruiti doua siruri de octeti:
;        - B1: contine ca elemente partea inferioara a cuvintelor inferioara din A
;        - B2: contine ca elemente partea superioara a cuvintelor superioare din A

segment data use32 class=data
    ; Variabile:

    a dd 12345607h, 1A2B3C15h, 13A33412h
    lenA equ ($-a)/4
    b1 times lenA db 0
    b2 times lenA db 0
    
    
segment code use32 class=code
    start:
        ; Instructiuni:
        
        mov esi, 0
        mov edi, 0
        mov ecx, lenA
        repeta1:
            mov al, byte [esi + a]
            mov [edi + b1], al
            add esi, 4
            inc edi
        loop repeta1
        
        mov esi, 3
        mov edi, 0
        mov ecx, lenA
        repeta2:
            mov al, byte [esi + a]
            mov [edi + b2], al
            add esi, 4
            inc edi
        loop repeta2
    
        push    dword 0
        call    [exit]