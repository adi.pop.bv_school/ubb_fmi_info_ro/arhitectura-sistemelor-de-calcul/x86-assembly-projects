bits 32

global start        

extern exit
import exit msvcrt.dll

; Enunt: Sa se creeze sirul oct inferiori div cu 2 din cuv infer din dw super  SI  sirul de cuv inf care au ultima acifra 4 din dw inferioare.

segment data use32 class=data
    ; Variabile:

    s dq 1223344554433221h
    lenS equ $-s
    a times lenS db 0
    b times lenS dw 0
    
segment code use32 class=code
    start:
        ; Instructiuni:
        cld
        mov esi, s
        add esi, 4
        
        mov edi, a
        add edi, 0
        
        mov ecx, lenS
        
        repeta1:
            lodsb
            mov dl, al
            cbw
            mov bl, 2
            idiv bl
            cmp ah, 0
            je par
            jmp final1
            par:
                mov al, dl
                stosb
            final1:
                add esi, 7
        loop repeta1
        
        mov esi, s
        mov edi, b
        mov ecx, lenS
        
        repeta2:
            lodsw
            mov dx, ax
            cbw
            mov bl, 10
            idiv bl
            cmp ah, 4
            je epatru
            jmp final2
            epatru:
                mov ax, dx
                stosw
            final2:
                add esi, 6
        loop repeta2
    
        push    dword 0
        call    [exit]