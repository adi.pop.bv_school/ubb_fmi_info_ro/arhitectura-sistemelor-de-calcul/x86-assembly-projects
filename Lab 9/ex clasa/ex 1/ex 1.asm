bits 32

global start        

extern exit, fopen, fclose, fprintf
import fopen msvcrt.dll
import fclose msvcrt.dll
import fprintf msvcrt.dll
import exit msvcrt.dll

; Enunt: se da un  nume de fisier si un nr zecimal >= 0 dw.
;        Sa se det si afis in fisier pe cate o linie separata cifrele zecimale si pe ultima lin suma lor in hexa.

segment data use32 class=data
    ; Variabile:

    numeFisier db 'rezultat.txt', 0
    numar dd 12345
    modAcces db 'w', 0
    formatZecimal db '%d', 0
    formatHexa db '%x', 0
    formatLinieNoua db 10, 0
    suma dd 0
    descriptor dd -1
    copie dd 0
    
segment code use32 class=code
    start:
        ; Instructiuni:
        
        push dword modAcces
        push dword numeFisier
        call [fopen]
        add esp, 4*2
        
        cmp eax, 0
        je final
        mov [descriptor], eax
        mov eax, [numar]
        
        repeta:
            mov edx, 0
            mov ebx, 10
            div ebx
            mov [copie], eax
            add [suma], edx
            
            push dword edx
            push dword formatZecimal
            push dword [descriptor]
            call [fprintf]
            add esp, 4*3
            
            push dword formatLinieNoua
            push dword [descriptor]
            call [fprintf]
            add esp, 4*2
            
            mov eax, [copie]
            cmp eax, 0
            jne repeta
            
        push dword [suma]
        push dword formatHexa
        push dword [descriptor]
        call [fprintf]
        add esp, 4*3
        
        push dword [descriptor]
        call [fclose]
        add esp, 4*1
            
        final:
    
        push    dword 0
        call    [exit]