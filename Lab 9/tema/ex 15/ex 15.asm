bits 32

global start        

extern exit, fopen, fprintf, fclose
import exit msvcrt.dll
import fopen msvcrt.dll
import fprintf msvcrt.dll
import fclose msvcrt.dll

; Enunt: Se dau un nume de fisier si un text (definite in segmentul de date). 
;        Textul contine litere mici, litere mari, cifre si caractere speciale. 
;        Sa se inlocuiasca toate caracterele speciale din textul dat cu caracterul 'X'. 
;        Sa se creeze un fisier cu numele dat si sa se scrie textul obtinut in fisier.

segment data use32 class=data
    ; Variabile:

    modAcces db 'w', 0
    numeFisier db 'rezultat.txt', 0
    formatCaracter db 'Sirul de caractere cerut este: %s', 0
    descriptor dd -1
    text db 'aA1!bB2&'
    lenT equ $-text
    
segment code use32 class=code
    start:
        ; Instructiuni:
        
        push dword modAcces
        push dword numeFisier
        call [fopen]
        add esp, 4*2
        
        cmp eax, 0
        je final
        mov [descriptor], eax
        
        mov ecx, lenT
        mov esi, 0
        
        caracter:
            mov al, byte [text + esi]
            cmp al, '!'
            jae ok
            jmp nuOk
            ok:
                cmp al, '/'
                ja nuOk
                mov al, 'X'
                mov byte [text + esi], al
            nuOk:
            inc esi
        loop caracter
        push dword text
        push dword formatCaracter
        push dword [descriptor]
        call [fprintf]
        add esp, 4*3
        
        push dword [descriptor]
        call [fclose]
        add esp, 4
        
        final:
        push    dword 0
        call    [exit]