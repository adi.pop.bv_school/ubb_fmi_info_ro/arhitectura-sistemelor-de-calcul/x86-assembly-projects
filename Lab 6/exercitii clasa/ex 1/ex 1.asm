bits 32 ; assembling for the 32 bits architecture

; declare the EntryPoint (a label defining the very first instruction of the program)
global start        

; declare external functions needed by our program
extern exit               ; tell nasm that exit exists even if we won't be defining it
import exit msvcrt.dll    ; exit is a function that ends the calling process. It is defined in msvcrt.dll
                          ; msvcrt.dll contains exit, printf and all the other important C-runtime specific functions

; our data is declared here (the variables needed by our program)
segment data use32 class=data
    ; ...

    ; se da sirul s. Sa se faca p si q care sa contina nr pozitive si respectiv negative al lui s. Sirul suma sa aiba suma elementelor celor 2 siruri p si q.
    
    s db 1,-1,0Ah,0FAh,0,2
    lenS equ $-s
    p times lenS db 0
    q times lenS db 0
    suma times lenS db 0
    
; our code starts here
segment code use32 class=code
    start:
        ; ...
        
        mov esi, 0
        mov edi, 0
        mov ebp, 0
        mov ecx, lenS
        mov dl, 0
        repeta1:
            cmp [s + esi], dl
            jge pozitiv
            jl negativ
            pozitiv:
                mov al, [s + esi]
                mov [p + edi], al
                inc edi
                jmp final
            negativ:
                mov al, [s + esi]
                mov [q + ebp], al
                inc ebp
            final:
                inc esi
        loop repeta1
        
        mov esi, 0
        mov ecx, lenS
        repeta2:
            mov al, [p + esi]
            add al, [q + esi]
            mov [suma  + esi], al
            inc esi
        loop repeta2
        
        ; exit(0)
        push    dword 0      ; push the parameter for exit onto the stack
        call    [exit]       ; call exit to terminate the program
