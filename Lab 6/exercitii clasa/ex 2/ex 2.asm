bits 32

global start        

extern exit
import exit msvcrt.dll

; Enunt: Sa se determine nr octetilor superiori divizibili cu 3.

segment data use32 class=data
    ; Variabile:

        s dw 1234h,5678h,1A2Bh
        lenS equ ($-s)/2
        nr db 0
        
segment code use32 class=code
    start:
        ; Instructiuni:
        
        mov esi, 1
        mov ecx, lenS
        repeta:
            mov al, [s + esi]
            cbw
            
            mov dl, 3
            div dl
            
            mov al, 0
            cmp ah, al
            je divizibil
            jmp final
            divizibil:
                inc byte[nr]
            final:
                add esi, 2
        loop repeta
        
        push    dword 0
        call    [exit]