bits 32

global start        

extern exit
import exit msvcrt.dll

; Enunt: Se da un sir dword. Sa se determ sirurile a si b care sa contina oct infer din cuv super si respectiv oct super din cuv infer.

segment data use32 class=data
    ; Variabile:

    s dd 12345678h, 1A2B3C4Dh, 11223344h
    lenS equ ($-s)/4
    a times lenS db 0
    b times lenS db 0
    
segment code use32 class=code
    start:
        ; Instructiuni:
        
        mov esi, 2
        mov edi, 1
        mov ebp, 0
        mov ecx, lenS
        repeta:
            mov al, [s + esi]
            mov [a + ebp], al
            
            mov al, [s + edi]
            mov [b + ebp], al
            
            add esi, 4
            add edi, 4
            inc ebp
        loop repeta
    
        push    dword 0
        call    [exit]