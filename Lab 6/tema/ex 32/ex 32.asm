bits 32

global start        

extern exit
import exit msvcrt.dll

; Enunt: Se da un sir de octeti S de lungime l. 
;        Sa se construiasca sirul D de lungime l-1 astfel incat elementele din D sa reprezinte catul dintre fiecare 2 elemente consecutive S(i) si S(i+1) din S. 

segment data use32 class=data
    ; Variabile:

    s db 1, 6, 3, 1
    lenS equ $-s
    d times lenS db 0
    
segment code use32 class=code   
    start:
        ; Instructiuni:
        
        mov esi, 0
        mov ecx, lenS - 1
        repeta:
            mov al, [s + esi]
            cbw
            div byte[s + esi +1]
            mov [d + esi], al
            inc esi
        loop repeta
        
        push    dword 0
        call    [exit]