bits 32

global start        

extern exit
import exit msvcrt.dll

; Enunt: Se dau sirurile a si b. 
;        Sa se faca sirul r cu elem lui b in ord inversa, urmate de elem lui a in ord inversa.

segment data use32 class=data
    ; Variabile:

    a db 2,1,-3,0
    lenA equ $-a
    b db 4,5,7,6,2,1
    lenB equ $-b
    r times lenA+lenB db 0
    
segment code use32 class=code
    start:
        ; Instructiuni:
        
        mov edi, 0
        
        mov ecx, lenB
        repetaB:
            mov al, [b + ecx - 1]
            mov [r + edi], al
            inc edi
        loop repetaB
        
        mov ecx, lenA
        repetaA:
            mov al, [a + ecx -1]
            mov [r + edi], al
            inc edi
        loop repetaA
        
        push    dword 0
        call    [exit]