bits 32

global start

extern exit 
import exit msvcrt.dll

segment data use32 class=data

    ;Se da un sir de octeti S. Sa se obtina sirul D1 ce contine toate numerele pare din S si sirul D2 ce contine toate numerele impare din S. 
    
    s db 1,5,3,8,2,9
    lenS equ $-s
    d1 times lenS db 0
    d2 times lenS db 0

segment code use32 class=code
    start:
        mov ebp, 0 ;s
        mov esi, 0 ;d1
        mov edi, 0 ;d2
        mov ecx, lenS

        repeta:
            mov al, [s + ebp]
            cbw
            mov dl, 2
            div dl
            cmp ah, 0
            je par
            jne impar
            par:
                mov al, [s + ebp]
                mov [d1 + esi], al
                inc esi
                jmp final
            impar:
                mov al, [s + ebp]
                mov [d2 + edi], al
                inc edi
                jmp final
            final:
                inc ebp
        loop repeta
    
        push dword 0
        call [exit]